import { observable, configure, flow } from 'mobx';
import Api from '../api/Api';

const api = new Api();
configure({ enforceActions: 'observed' });

class PokemonsStore {
  @observable pokemons = [];

  @observable fetchState = 'pending'; // pending, done, error

  @observable searchKey = '';

  @observable showOnPage = 10;

  updateSearch(searchKey) {
    this.searchKey = searchKey.trim().toLowerCase();
  }

  fetchPokemons = flow(function* () {
    this.pokemons = [];
    this.fetchState = 'pending';
    try {
      const { results: pokemons } = yield api.getPokemons();
      const pokemonsDataPromises = [];
      for (let i = 0; i < this.showOnPage; i += 1) {
        const pokemon = pokemons[i];
        pokemonsDataPromises.push(api.getPokemon(pokemon.name));
      }
      const pokemonsData = yield Promise.all(pokemonsDataPromises);
      this.fetchState = 'done';
      this.pokemons = pokemonsData;
    } catch (error) {
      this.fetchState = 'error';
    }
  });

  filterPokemons() {
    return this.pokemons.filter((pokemon) => {
      const name = pokemon;
      if (this.searchKey.length === 0) {
        return true;
      }
      return name.indexOf(this.searchKey) > -1;
    });
  }
}

export default new PokemonsStore();
